<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Owner;
use App\Entity\Region;
use App\Entity\Room;
use App\Form\RoomType;
use App\Repository\RoomRepository;


/**
 * Controleur Miniprojet
 * 
 * @Route("/room")
 */

class MiniProjetController extends AbstractController
{
    /**
     * @Route("/", name = "mini_projet", methods="GET")
     */
    public function index(): Response
    {
        return $this->render('base.html.twig', 
        [ 'controller_name' => 'MiniProjetController']);
    }

    /**
     * Lists all Rooms by Region.
     *
     * @Route("/list", name="room_list", methods="GET")
     */

    public function listAction(){

        $em = $this->getDoctrine()->getManager();
        $rooms = $em->getRepository(Room::class)->findAll();

        dump($rooms);

        return $this->render('mini_projet/index.html.twig',
        [ 'rooms' => $rooms ]);
    }

    /**
     * Finds and displays a room entity.
     *
     * @Route("/{id}", name="room_show", requirements={ "id": "\d+"}, methods="GET")
     */

    public function showAction(Room $room): Response
    {
        return $this->render('mini_projet/show.html.twig', [ 
            'room' => $room,
        ]);
    }

    /**
     * @Route("/new", name="room_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $room = new Room();
        $form = $this->createForm(RoomType::class, $room);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($room);
            $entityManager->flush();
            
            // Make sure message will be displayed after redirect
            $this->get('session')->getFlashBag()->add('message', 'bien ajouté');
            
            return $this->redirectToRoute('room_list', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('mini_projet/new.html.twig', [
            'room' => $room,
            'form' => $form->createView(),
        ]);
    }


    /**
     * @Route("/{id}/edit", name="room_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Room $room): Response
    {
        $form = $this->createForm(RoomType::class, $room);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('room_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('mini_projet/edit.html.twig', [
            'room' => $room,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/delete/{id}", name="room_delete", methods={"POST"})
     */
    public function delete(Request $request, Room $room): Response
    {
        if ($this->isCsrfTokenValid('delete'.$room->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($room);
            $entityManager->flush();
        }

        return $this->redirectToRoute('room_list', [], Response::HTTP_SEE_OTHER);
    }

    /**
     * Adds a room to the bag
     * 
     * @Route("/mark/{id}", name="room_mark", requirements={ "id": "\d+"}, methods="GET")
     */
    public function markAction(Room $room): Response
    {
        if($this->get('session')->get('panier')) { 
            $panier = $this->get('session')->get('panier');
            if (! in_array($room->getId(), $panier) ) {
                $panier->append($room->getId()); 
            } else {
                $panier = array_diff($panier, array($room->getId()));
            }

        } else {
            $panier = array($room->getId());
            
        }

        $this->get('session')->set('panier', $panier);


        dump($room);
        return $this->redirectToRoute('room_show', 
        ['id' => $room->getId()]);
    }

}
